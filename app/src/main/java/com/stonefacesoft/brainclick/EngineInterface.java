package com.stonefacesoft.brainclick;

/**
 * Created by Héctor on 24/1/2017.
 */

public interface EngineInterface {

    //train
    public void trainStarted();
    public void trainSucceed();
    public void trainCompleted();
    public void trainRejected();
    public void trainErased();
    public void trainReset();
    public void userAdded(int userId);
    public void userRemove();

    // detection
    public void detectedActionLowerFace(int typeAction,float power);
    // detection
    public void detectedActionUpperFace(int typeAction,float power);

    //headset loggin
    public void headsetStatus(int[] BatteryLevel, int[] ContactQ);

}