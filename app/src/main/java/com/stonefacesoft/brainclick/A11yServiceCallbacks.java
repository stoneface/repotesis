package com.stonefacesoft.brainclick;

import android.app.Activity;
import android.content.Context;
import android.os.SystemClock;
import android.switchaccess.Analytics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.view.inputmethod.BaseInputConnection;

import com.android.switchaccess.ActionProcessor;
import com.android.switchaccess.KeyboardEventManager;
import com.emotiv.insight.IEmoStateDLL;

import java.io.IOException;
///TODO puede fallar el analytics aca

/**
 * Created by chrismcmeeking on 2/21/17.
 */

public class A11yServiceCallbacks implements EngineInterface {

    private KeyboardEventManager mKeyboardEventManager;
    private ActionProcessor mActionProcessor;
    private Analytics mAnalytics;
    //Variables del Emotiv
    int userId = 0;
    private EngineConnector engineConnector;
    private Context mContext;

    public A11yServiceCallbacks(Context context, KeyboardEventManager keyboardEventManager, ActionProcessor actionProcessor, Analytics analytics) {
        engineConnector = EngineConnector.shareInstance();
        engineConnector.delegate = this;
        mActionProcessor = actionProcessor;
        mAnalytics = analytics;
        mKeyboardEventManager = keyboardEventManager;
        mContext = context;
    }

    @Override
    public void trainStarted() {

    }

    @Override
    public void trainSucceed() {

    }

    @Override
    public void trainCompleted() {

    }

    @Override
    public void trainRejected() {

    }

    @Override
    public void trainErased() {

    }

    @Override
    public void trainReset() {

    }

    @Override
    public void userAdded(int userId) {
        Log.e("A11yService","User added");
        this.userId = userId;
    }

    @Override
    public void userRemove() {
        Log.e("A11yService","User Removed");
        this.userId = -1;
    }

    @Override
    public void detectedActionLowerFace(int typeAction, float power) {

    }

    @Override
    public void detectedActionUpperFace(int typeAction, float power) {
        if (typeAction == IEmoStateDLL.IEE_FacialExpressionAlgo_t.FE_SURPRISE.ToInt()) {
            if (power >= 0.5) {
                //TODO genero el accesibility event
                AccessibilityManager manager = (AccessibilityManager) mContext.getSystemService(Context
                        .ACCESSIBILITY_SERVICE);
                if (manager.isEnabled()) {
                    AccessibilityEvent event = AccessibilityEvent.obtain();
                    event.setEventType(AccessibilityEvent.TYPE_ANNOUNCEMENT);
                    event.setClassName(getClass().getName());
                    event.getText().add("Fire");

                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                        event.setSource(((Activity) mContext).getWindow().getDecorView()
                                .findViewById(R.id.BtnOk));
                    }
                    manager.sendAccessibilityEvent(event);
                }
            }
        }
    }

    @Override
    public void headsetStatus(int[] BatteryLevel, int[] ContactQ) {

    }
}
