//package com.stonefacesoft.brainclick;
//
//import android.accessibilityservice.AccessibilityService;
//
//import android.app.AlertDialog;
//import android.content.Context;
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.content.SharedPreferences;
//import android.os.Build;
//import android.os.Handler;
//import android.os.PowerManager;
//import android.support.annotation.RequiresApi;
//import android.support.v4.os.BuildCompat;
//import android.util.Log;
//import android.view.KeyEvent;
//import android.view.accessibility.AccessibilityEvent;
//import android.view.accessibility.AccessibilityManager;
//import android.widget.Toast;
//
//import com.emotiv.insight.FacialExpressionDetection;
//import com.emotiv.insight.IEmoStateDLL;
//
//import java.util.Timer;
//import java.util.TimerTask;
//
//import talkback.TalkBackService;
//import testswitchaccess.ActionProcessor;
//import testswitchaccess.Analytics;
//import testswitchaccess.AutoScanController;
//import testswitchaccess.GlobalActionNode;
//import testswitchaccess.KeyboardEventManager;
//import testswitchaccess.OptionManager;
//import testswitchaccess.OptionScanNode;
//import testswitchaccess.OverlayController;
//import testswitchaccess.SwitchAccessService;
//import testswitchaccess.SwitchAccessWindowInfo;
//import testswitchaccess.UiChangeDetector;
//import testswitchaccess.treebuilding.MainTreeBuilder;
//import testswitchaccess.utils.LogUtils;
//import testswitchaccess.utils.SharedPreferencesUtils;
//import testswitchaccess.utils.widget.SimpleOverlay;
//
//import static com.stonefacesoft.brainclick.EngineConnector.context;
//
///**
// * Created by Héctor on 13/1/2017.
// */
//
////https://github.com/google/talkback/tree/12bdf2063e121a021f050c94cf5ebb2489c8af8a/src/main/java/com/android/switchaccess
//
//public class ClickerService implements EngineInterface,
//        SharedPreferences.OnSharedPreferenceChangeListener,
//        ActionProcessor.UiChangedListener{
//
//    //Variables del Emotiv
//    int userId = 0,count = 0,actionSelected = 555;
//    EngineConnector engineConnector;
//    public static float _currentPower = 0;
//    boolean isTrainning = false;
//    Timer timer;
//    TimerTask timerTask;
//    String currentRunningAction="";
//
//    Notifications notificador;
//
//    private ActionProcessor mActionProcessor;
//    private UiChangeDetector mEventProcessor;
//
//    private Analytics mAnalytics;
//    private PowerManager.WakeLock mWakeLock;
//    private static ClickerService sInstance = null;
//    private OverlayController mOverlayController;
//    private OptionManager mOptionManager;
//    private AutoScanController mAutoScanController;
//    private KeyboardEventManager mKeyboardEventManager;
//    private MainTreeBuilder mMainTreeBuilder;
//
//
//    public ClickerService() {
//        super();
//    }
//
//    @Override
//    public void onCreate() {
//        super.onCreate();
//
//        //Iniciando la conexion
//        engineConnector = EngineConnector.shareInstance();
//        engineConnector.delegate = this;
//
//        if (engineConnector.isConnected) {
//            Log.e("ClickerService", "Conectado userId: " + userId);
//        } else
//            Log.e("ClickerService","No Conectado");
//    }
//
//
//    @Override
//    public boolean onUnbind(Intent intent) {
//        if (mAutoScanController != null) {
//            mAutoScanController.stopScan();
//        }
//        return super.onUnbind(intent);
//    }
//
//    @Override
//    public void onDestroy() {
//        mAnalytics.stop();
//        mOptionManager.shutdown();
//        mOverlayController.shutdown();
//        mMainTreeBuilder.shutdown();
//        sInstance = null;
//        super.onDestroy();
//    }
//
//
//    @Override
//    protected void onServiceConnected() {
//        super.onServiceConnected();
//        notificador = new Notifications();
//        notificador.SetContext(getBaseContext());
//        Toast.makeText(getApplicationContext(), R.string.StrOnServiceConn,Toast.LENGTH_SHORT).show();
//
//
//        //SwitchAccess
//        sInstance = this;
//        mOverlayController = new OverlayController(new SimpleOverlay(this));
//        mOverlayController.configureOverlay();
//        mOptionManager = new OptionManager(mOverlayController);
//        mMainTreeBuilder = new MainTreeBuilder(this);
//        mAutoScanController = new AutoScanController(mOptionManager, new Handler(), this);
//        mKeyboardEventManager = new KeyboardEventManager(this, mOptionManager, mAutoScanController);
//        mAnalytics = new Analytics();
//        mAnalytics.start();
//        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
//        mWakeLock = powerManager.newWakeLock(
//                PowerManager.FULL_WAKE_LOCK | PowerManager.ON_AFTER_RELEASE, "ClickerService");
//        SharedPreferencesUtils.getSharedPreferences(this)
//                .registerOnSharedPreferenceChangeListener(this);
//        mActionProcessor = new ActionProcessor(this);
//        mEventProcessor = new UiChangeDetector(mActionProcessor);
//    }
//
//
//    @Override
//    public void trainStarted() {
//        Log.e("ClickerService","TrainStarted");
//    }
//
//    @Override
//    public void trainSucceed() {
//        Log.e("ClickerService","TrainSucceed");
//        new AlertDialog.Builder(this)
//                .setTitle("Training Succeeded")
//                .setMessage("Training is successful. Accept this training?")
//                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        // continue with delete
//                        engineConnector.setTrainControl(FacialExpressionDetection.IEE_FacialExpressionTrainingControl_t.FE_ACCEPT.getType());
//                    }
//                })
//                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        // do nothing
//                        engineConnector.setTrainControl(FacialExpressionDetection.IEE_FacialExpressionTrainingControl_t.FE_REJECT.getType());
//                    }
//                })
//                .setIcon(android.R.drawable.ic_dialog_alert)
//                .show();
//    }
//
//    @Override
//    public void trainCompleted() {
//        Log.e("ClickerService","TrainCompleted");
//
//    }
//
//    @Override
//    public void trainRejected() {
//        Log.e("ClickerService","Train Rejected");
//    }
//
//    @Override
//    public void trainErased() {
//        Log.e("ClickerService","Train Erased");
//    }
//
//    @Override
//    public void trainReset() {
//        Log.e("ClickerService","Train Reset");
//    }
//
//    @Override
//    public void userAdded(int userId) {
//        Log.e("ClickerService","User added");
//        this.userId = userId;
//    }
//
//    @Override
//    public void userRemove() {
//        Log.e("ClickerService","User Removed");
//        this.userId = -1;
//    }
//
//    @Override
//    public void detectedActionLowerFace(int typeAction, float power) {
//
//    }
//
//    @Override
//    public void detectedActionUpperFace(int typeAction, float power) {
//
//        if (typeAction == IEmoStateDLL.IEE_FacialExpressionAlgo_t.FE_SURPRISE.ToInt()) {
//            if (power >= 0.7) {
//                //TODO dispatch accesibility event
//                AccessibilityManager manager = (AccessibilityManager) getApplicationContext()
//                        .getSystemService(Context.ACCESSIBILITY_SERVICE);
//                if (manager.isEnabled()) {
//                    AccessibilityEvent e = AccessibilityEvent.obtain();
//                    e.setEventType(AccessibilityEvent.TYPE_ANNOUNCEMENT);
//                    e.setClassName(getClass().getName());
//                    e.setPackageName(getApplicationContext().getPackageName());
//                    e.setContentDescription("Fire");
//                    manager.sendAccessibilityEvent(e);
//
//                    KeyEvent ke = new KeyEvent(1000, "C", 0, KeyEvent.ACTION_DOWN);
//
//                    mKeyboardEventManager.onKeyEvent(ke, mActionProcessor, mAnalytics);
//                }
//            }
//        }
//    }
//
//    @Override
//    public void headsetStatus(int[] BatteryLevel, int[] ContactQ) {
////        Log.e("TrainActivity","Contact Q :"+ContactQ.length);
////        Log.e("TrainActivity","Battery level :"+BatteryLevel[0]);
//        try {
//            notificador.NotificarLowQ(ContactQ);
//            notificador.NotificarBateriaBaja(BatteryLevel);
//        }catch (NullPointerException e) {
//            Log.e("TrainActivity", e.toString());
//        }
//    }
//
//    //SwitchAccess
//    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
//    @Override
//    public void onAccessibilityEvent(AccessibilityEvent event) {
//        int eventType = event.getEventType();
//        switch (eventType) {
//            //Representa el evento de abrir un PopupWindow, Menu, Dialog, etc.
//            case AccessibilityEvent.TYPE_ANNOUNCEMENT:
//                //TODO sacar esto q esta para comprobar
//                Toast.makeText(getApplicationContext(), event.getContentDescription(), Toast.LENGTH_SHORT)
//                        .show();
//                if (event.getContentDescription() == "Fire") {
//                    Log.e("ClickerService", "Accesibility event");
//                    Toast.makeText(getApplicationContext(), "Exito", Toast.LENGTH_SHORT)
//                            .show();
//                    boolean AccKeyHandled = onKeyEventShared(event);
//                    if (AccKeyHandled) {
//                        rebuildOptionScanTree();
//                    }
//                    break;
//                }
//        }
//        mEventProcessor.onAccessibilityEvent(event);
//    }
//
//    @Override
//    public void onInterrupt() {
//        /* TODO Will this ever be called? */
//    }
//
//    /**
//     * @return The active SwitchingControl instance or {@code null} if not available.
//     */
//    public static ClickerService getInstance() {
//        return sInstance;
//    }
//
//    /**
//     * Intended to mimic the behavior of onKeyEvent if this were the only service running.
//     * It will be called from onKeyEvent, both from this service and from others in this apk
//     * (TalkBack). This method must not block, since it will block onKeyEvent as well.
//     * @param keyEvent A key event
//     * @return {@code true} if the event is handled, {@code false} otherwise.
//     */
//    public boolean onKeyEventShared(KeyEvent keyEvent) {
//        if (mKeyboardEventManager.onKeyEvent(keyEvent, mActionProcessor, mAnalytics)) {
//            mWakeLock.acquire();
//            mWakeLock.release();
//            return true;
//        }
//        return false;
//    }
//
//    //Cambios hector
//    public boolean onKeyEventShared(AccessibilityEvent keyEvent) {
//        if (mKeyboardEventManager.onKeyEvent(keyEvent, mActionProcessor, mAnalytics)) {
//            mWakeLock.acquire();
//            mWakeLock.release();
//            return true;
//        }
//        return false;
//    }
//    //cambios hector
//
//
//    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
//    @Override
//    protected boolean onKeyEvent(KeyEvent keyEvent) {
//        boolean keyHandled = onKeyEventShared(keyEvent);
//        if (keyHandled) {
//
//            rebuildOptionScanTree();
//        }
//        if (!BuildCompat.isAtLeastN()) {
//            TalkBackService talkBackService = TalkBackService.getInstance();
//            if (talkBackService != null) {
//                keyHandled = talkBackService.onKeyEventShared(keyEvent) || keyHandled;
//            }
//        }
//        return keyHandled;
//    }
//
//    @Override
//    public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
//        LogUtils.log(this, Log.DEBUG, "A shared preference changed: %s", key);
//        mKeyboardEventManager.reloadPreferences(this);
//    }
//
//    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
//    @Override
//    public void onUiChangedAndIsNowStable() {
//        rebuildOptionScanTree();
//    }
//
//    public OptionManager getOptionManager() {
//        return mOptionManager;
//    }
//
//    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
//    private void rebuildOptionScanTree() {
//        Log.e("ClickerService"," rebuildOptionScanTree");
//        OptionScanNode globalContextMenuTree =
//                mMainTreeBuilder.buildContextMenu(GlobalActionNode.getGlobalActionList(this));
//        mOptionManager.clearFocusIfNewTree(mMainTreeBuilder.addWindowListToTree(
//                SwitchAccessWindowInfo.convertZOrderWindowList(getWindows()),
//                globalContextMenuTree));
//    }
//}
