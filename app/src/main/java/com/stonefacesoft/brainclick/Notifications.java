package com.stonefacesoft.brainclick;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by Héctor on 31/1/2017.
 */

public class Notifications {

    Context context;

    public void SetContext(Context con){
        this.context = con;
    }

    //notificació cuando la bateria se está acabando TODO uso iconos estandar, ver si vale la
    // pena poner algun otro
    public void NotificarBateriaBaja(int[] Battery) {
        if (Battery[0]<2) {
            Bitmap icon = BitmapFactory.decodeResource(context.getResources(),
                    android.R.drawable.ic_lock_idle_low_battery);
            Notification noti = new Notification.Builder(context)
                    .setContentTitle(context.getString(R.string.NotiLowBat))
                    .setContentText(context.getString(R.string.notiLowBatTet))
                    .setSmallIcon(android.R.drawable.ic_lock_idle_low_battery)
                    .setLargeIcon(icon)
                    .setPriority(Notification.PRIORITY_DEFAULT)
                    .setLights(Color.RED, 500, 500)
                    .build();

            // Sets an ID for the notification
            int mNotificationId = 1;
            // Gets an instance of the NotificationManager service
            NotificationManager mNotifyMgr =
                    (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
            // Builds the notification and issues it.
            mNotifyMgr.notify(mNotificationId, noti);
        }
    }

    //notificación cuando ContactQ es muy baja
    public void NotificarLowQ(int[] Data) {
        float ContactQValue = (float)((Data[3]+Data[16]+Data[7]+Data[12]+Data[9])/5)/4*100;
        if (ContactQValue <= 50.0) {
            Bitmap icon = BitmapFactory.decodeResource(context.getResources(),
                    android.R.drawable.ic_dialog_alert);
            Notification noti = new Notification.Builder(context)
                    .setContentTitle(context.getString(R.string.NotiLowQ))
                    .setContentText(context.getString(R.string.NotiLowQText))
                    .setSmallIcon(android.R.drawable.ic_dialog_alert)
                    .setLargeIcon(icon)
                    .setPriority(Notification.PRIORITY_HIGH)
                    .build();
            // Sets an ID for the notification
            int mNotificationId = 2;
            // Gets an instance of the NotificationManager service
            NotificationManager mNotifyMgr =
                    (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
            // Builds the notification and issues it.
            mNotifyMgr.notify(mNotificationId, noti);
        }
    }

    //TODO ver si es util hacer una notificacion permanente con esta informacion o un blob que
    // este siempre presente


}
